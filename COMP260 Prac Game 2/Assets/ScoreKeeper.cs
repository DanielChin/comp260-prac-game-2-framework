﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreKeeper : MonoBehaviour {

	// Use this for initialization
	public int scorePerGoal = 1;
	private int[] score = new int[2];
	public Text[] scoreText;
	public Text winText;
	private bool winner;

	void Start () {    
		// subscribe to events from all the Goals
		Goal[] goals = FindObjectsOfType<Goal> ();

		for (int i = 0; i < goals.Length; i++) {
			goals[i].scoreGoalEvent += OnScoreGoal;
		}

		for (int i = 0; i < score.Length; i++) {
			score [i] = 0;
			scoreText [i].text = "0";
		}

		winner = false;
	}

	public void OnScoreGoal(int player) {
		if (winner == false) {
			// add points to the player whose goal it is
			score [player] += scorePerGoal;
			scoreText [player].text = score [player].ToString ();
			Debug.Log ("Player " + player + ": " + score [player]);

			if (score [player] >= 10 && winner == false) {
				winText.text = "Player " + (player + 1) + " wins";
				winner = true;
			}
		}
	}
}
