﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioManager : MonoBehaviour {

	private int[] score = new int[3];
	private bool winner;

	public AudioSource audio;

	public static AudioManager Instance {
		get;
		private set;
	}

	void Awake () {
		Instance = this;
	}

	void Start () {    
		// subscribe to events from all the Goals'
		audio = GetComponent<AudioSource> ();


		//PuckControl[] sounds1 = FindObjectsOfType<PuckControl> ();
		//Goal[] sounds2 = FindObjectsOfType<Goal> ();

		//for (int i = 0; i < sounds1.Length; i++) {
		//	sounds1[i].SoundEvent += Sound;
		//}
		//for (int i = 0; i < sounds2.Length; i++) {
		//	sounds2[i].SoundEvent += Sound;
		//}

	}

	public void Sound(AudioClip clip) {
		audio.PlayOneShot(clip);
	}
}
