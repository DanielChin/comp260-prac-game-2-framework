﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PuckControl : MonoBehaviour {

	public AudioClip wallCollideClip;
	public AudioClip paddleCollideClip;
	private AudioSource audio;

	public Transform startingPos;
	private Rigidbody rigidbody;

	public delegate void SoundHandler(AudioClip clip);
	public SoundHandler SoundEvent;

	void Start () {
		audio = GetComponent<AudioSource> ();

		rigidbody = GetComponent<Rigidbody>();
		ResetPosition();
	}

	// Update is called once per frame
	void Update () {
		
	}

	public LayerMask paddleLayer;

	void OnCollisionEnter(Collision collision) {
		Debug.Log("Collision Enter: " + collision.gameObject.name);
		if (paddleLayer.Contains(collision.gameObject)) {
			// hit the paddle
			AudioManager.Instance.Sound (paddleCollideClip);
		}
		else {
			// hit something else
			AudioManager.Instance.Sound (wallCollideClip);
		}
	}

	void OnCollisionStay(Collision collision) {
		Debug.Log("Collision Stay: " + collision.gameObject.name);
	}

	void OnCollisionExit(Collision collision) {
		Debug.Log("Collision Exit: " + collision.gameObject.name);
	}

	public void ResetPosition(){
		rigidbody.MovePosition(startingPos.position);
		rigidbody.velocity = Vector3.zero;
	}
}
