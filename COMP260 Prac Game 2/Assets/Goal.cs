﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Goal : MonoBehaviour {
		
	public delegate void ScoreGoalHandler(int player);
	public ScoreGoalHandler scoreGoalEvent;
	public delegate void SoundHandler(AudioClip clip);
	public SoundHandler SoundEvent;
	public int player;

	public AudioClip scoreClip;
	private AudioSource audio;

	void Start(){
		audio = GetComponent<AudioSource> ();
	}

	void OnTriggerEnter(Collider collider){


		//audio.PlayOneShot(scoreClip);
		PuckControl puck = collider.gameObject.GetComponent<PuckControl> ();

		puck.ResetPosition();

		if (scoreGoalEvent != null) {
			scoreGoalEvent (player);
		}
		/*
		if (SoundEvent != null) {
			SoundEvent (scoreClip);
		}
		*/
		AudioManager.Instance.Sound (scoreClip);
	}
}